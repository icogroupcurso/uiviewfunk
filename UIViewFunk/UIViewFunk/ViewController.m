//
//  ViewController.m
//  UIViewFunk
//
//  Created by Andres Torres on 8/7/14.
//  Copyright (c) 2014 Andres Torres. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    
/* Si el puntico esta relleno es que esta asociado a un objeto en la vista,
 * si esta vacio es porque no esta asignado */
/* <- */__weak IBOutlet UILabel *labelFunk;
    __weak IBOutlet UILabel *resultLabel;
    __weak IBOutlet UITextField* textFieldSuperior,
                               * textFieldInferior;
    
    __weak IBOutlet UIView *coloredView;
    
}

// IBAction simboliza que es una accion de un boton, no un parametro de return.
- (IBAction)changeFunkLabelText:(UIButton *)sender;
- (IBAction)changeColoredViewColor:(UIButton *)sender;


@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
        
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //Esto se pone para que se esconda el teclado al hacer click fuera del textField
    [textFieldInferior resignFirstResponder];
    [textFieldSuperior resignFirstResponder];
}

#pragma mark - Metodos de botones en la vista

/**
 * Metodo que se conecta al boton en el view y se ejecuta cuando lo tocan
 * @params sender - El boton se envia a si mismo como parametro del metodo
 *
 * IBAction simboliza que es una accion de un boton, no un parametro de return.
 */
- (IBAction)changeFunkLabelText:(UIButton *)sender {
    
    [labelFunk setText:@"Nuevo texto del label"];
//    Forma alternativa de hacer lo mismo
//    labelFunk.text = @"Nuevo texto";
    
    NSInteger numberSuperior = [textFieldSuperior text].integerValue; //.integerValue retorna el string como integer si es posible, retorna 0 si no es posible.
    //Forma alternartiva de hacer lo mismo
    //NSInteger number = [[textFieldSuperior text] integerValue];
    
    NSInteger numberInferior = [[textFieldInferior text] integerValue];
    
    NSString* someString = [NSString stringWithFormat:@"%li",(long)(numberSuperior+numberInferior)];
    
    [resultLabel setText:someString];
    
    
    
}

- (IBAction)changeColoredViewColor:(UIButton *)sender {

    UIColor* blueColor = [UIColor blueColor];
    
    //Este metodo le cambia el color de fondo al view al darle click al boton
    [coloredView setBackgroundColor:blueColor];
    //Forma alternativa
    //coloredView.backgroundColor = blueColor;
    
    //Esto crea un objeto que representa una nueva posicion y tamaño en la pantalla
    CGRect nuevoTamanoDeColoredView = CGRectMake(80, 317 + 193/4, 160, 193/2);
    
    //Aqui estamos asignando el nuevo tamaño a coloredView
    [coloredView setFrame:nuevoTamanoDeColoredView];
    
}

#pragma mark Fin de Metodos de botones en la vista -


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
